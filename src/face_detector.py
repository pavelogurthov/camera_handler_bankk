import dlib
import cv2
import uuid


class Detector:
    def __init__(self, frames_between_detections=30):
        self.FRAME_SIZE = 400.
        self.detector = dlib.get_frontal_face_detector()
        self.trackers = dict()
        self.INTERSECT_THRESHOLD = 0
        self.TIME_DETECT = frames_between_detections
        self.TIME_TO_DELETE_TRACKER = 30
        self.time = 0

    def gen_id(self):
        return uuid.uuid4()

    def face_detection(self, frame):
        gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        dets = self.detector(gray_frame, 0)
        return dets

    def is_intersect_rects(self, rect1, rect2):
        return rect1.intersect(rect2).area() > self.INTERSECT_THRESHOLD

    def search_intersect_tracker(self, detect):
        for key, tracker in self.trackers.iteritems():
            d = tracker[0].get_position()
            d = dlib.rectangle(int(d.left()), int(d.top()), int(d.right()), int(d.bottom()))
            if self.is_intersect_rects(detect, d):
                return tracker
        return None

    def select_tracker(self, x, y):
        for key, tracker in self.trackers.iteritems():
            d = tracker[0].get_position()
            if d.contains(x, y):
                self.tracker_for_enroll = tracker
                return key
        return None

    def delete_excess_trackers(self):
        tmp_trackers = dict()
        for key, t in self.trackers.iteritems():
            if t[1] < self.TIME_TO_DELETE_TRACKER:
                tmp_trackers[key] = t

        for key, t in self.trackers.iteritems():
            pos = t[0].get_position()
            new_trackers = dict()
            for key2, t2 in tmp_trackers.iteritems():
                pos2 = t2[0].get_position()
                if self.is_intersect_rects(pos2, pos) and pos.area() > pos2.area():
                    continue
                new_trackers[key2] = t2
            tmp_trackers = new_trackers
        self.trackers = tmp_trackers

    def update_trackers(self, frame):
        self.time += 1
        if self.time % self.TIME_DETECT == 0:
            dets = self.face_detection(frame)
            for d in dets:
                res = self.search_intersect_tracker(d)
                if None == res:
                    tracker = dlib.correlation_tracker()
                    tracker.start_track(frame, d)
                    self.trackers[self.gen_id()] = [tracker, 0]
                else:
                    res[0].update(frame, d)
                    res[1] = 0

        self.delete_excess_trackers()

    def get_pos_from_id(self, person_id):
        track = self.trackers.get(person_id)
        if None == track:
            return None
        return track[0].get_position()

    def resize_drect(self, rect):
        # d_x = (rect.right() - rect.left())/8.
        # d_y = (rect.bottom() - rect.top())/8.

        return rect
        # return dlib.drectangle(rect.left() - d_x, rect.top() - d_y, rect.right()+d_x, rect.bottom() + d_y)

    def get_detected(self, frame):
        self.update_trackers(frame)

        positions = dict()
        for key, track in self.trackers.iteritems():
            track[0].update(frame)
            track[1] += 1
            positions[key] = (track[0].get_position())
        return positions
